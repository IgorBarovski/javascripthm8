let title = document.createElement('title');
title.innerHTML = 'Call to Action 3';

let metaUtf8 = document.createElement('meta');
metaUtf8.setAttribute('charset', 'UTF-8');

document.head.appendChild(title);
document.head.appendChild(metaUtf8);
document.documentElement.setAttribute('lang', 'en');
// *****************************************************************************

let headerBlock=document.createElement('div');
document.body.appendChild(headerBlock);
headerBlock.classList.add('header__block')

let header = document.createElement('h1');
header.innerHTML = 'Choose Your Option';
headerBlock.appendChild(header);

let blocklWrapper=document.createElement('div');
document.body.appendChild(blocklWrapper);
blocklWrapper.classList.add('block__wrapper')

let p=document.createElement('p');
p.innerHTML='But I must explain to you how all this mistaken idea of denouncing ';
headerBlock.appendChild(p);
p.classList.add('heder__text')

let blockFreelance=document.createElement('div');
blocklWrapper.appendChild(blockFreelance);
blockFreelance.classList.add('block__freelance')

let spanFreelance=document.createElement('span');
spanFreelance.innerHTML='FREELANCER';
blockFreelance.appendChild(spanFreelance);
spanFreelance.classList.add('span__freelance')

let divFreelance=document.createElement('div');
divFreelance.innerHTML='Initially designed to ';
blockFreelance.appendChild(divFreelance);
divFreelance.classList.add('div__freelance')

let pFreelance=document.createElement('p');
pFreelance.innerHTML='But I must explain to you how all this mistaken idea of denouncing  ';
blockFreelance.appendChild(pFreelance);
pFreelance.classList.add('p__freelance')

let buttom=document.createElement('buttom');
buttom.innerHTML='START HERE ';
blockFreelance.appendChild(buttom);
buttom.classList.add('buttom')

// ********************************************************************************************

let blockStudio=document.createElement('div');
blocklWrapper.appendChild(blockStudio);
blockStudio.classList.add('block__studio')

let spanStudio=document.createElement('span');
spanStudio.innerHTML='STUDIO';
blockStudio.appendChild(spanStudio);
spanStudio.classList.add('span__studio')

let divStudio=document.createElement('div');
divStudio.innerHTML='Initially designed to ';
blockStudio.appendChild(divStudio);
divStudio.classList.add('div__studio')

let pStudio=document.createElement('p');
pStudio.innerHTML='But I must explain to you how all this mistaken idea of denouncing  ';
blockStudio.appendChild(pStudio);
pStudio.classList.add('p__studio')

let buttomStudio=document.createElement('buttom');
buttomStudio.innerHTML='START HERE ';
blockStudio.appendChild(buttomStudio);
buttomStudio.classList.add('buttom__studio')



let style =document.createElement('style');
document.head.appendChild(style);


style.innerHTML=

    `
    h1 {
        font-family: 'Arvo';
        font-style: normal;
        font-weight: 400;
        font-size: 36px;
        line-height: 48px;
        color: #212121;
        text-align: center;
        
    }
    .heder__text{
        font-family: 'OpenSans';
        font-size: 14px;
        line-height: 26px;
        text-align: center;
        color: #9FA3A7;
    }
    .block__wrapper{
        display: flex;
        justify-content: center;
        margin-top:55px;
    }
    .block__freelance, .block__studio{
        text-align: center;
        width: 401px;
        height: 408px;
        border: 1px solid #E8E9ED;
    }
    .block__freelance:hover, .block__studio:hover{
        box-shadow: 5px 2px 15px 4px #efd6f7;;
    }
    .block__studio{
        background: #8F75BE;
    }
    .span__freelance, .span__studio{
        font-family: 'Montserrat';
        font-style: normal;
        font-weight: 700;
        font-size: 12px;
        line-height: 15px;
        text-align: center;
        letter-spacing: 2.4px;
        color: #9FA3A7;
        margin: 81px 147px 0px 147px;
        display: inline-block;
    }
    .span__studio{
        color: #FFC80A;
    }
    .div__freelance,.div__studio{
        width: 210px;
        height: 92px;
        font-family: 'Arvo';
        font-style: normal;
        font-weight: 400;
        font-size: 36px;
        line-height: 46p
        text-align: center;
        color: #212121;
        align-items: center;
        padding: 15px  96px 0px 96px;
        
    }
  
    .p__freelance,.p__studio{
        width: 210px;
        height: 44px;
        font-family: 'OpenSans';
        font-size: 12px;
        line-height: 22px;
        align-items: center;
        text-align: center;
        color: #9FA3A7;
        margin:25px 96px 52px 96px;
    }
    .buttom,.buttom__studio{
        width: 147px;
        height: 46px;
        display: inline-block;
        font-family: 'Montserrat';
        font-style: normal;
        font-weight: 700;
        font-size: 12px;
        line-height: 46px;
        text-align: center;
        letter-spacing: 2.4px;
        color: #212121;
        border: 3px solid #FFC80A;
        border-radius:30px;
        margin: 0px 127px 93px 127px;
    }
    .buttom:hover,.buttom__studio:hover{
        background:#FFC80A;
        cursor:pointer;
    }
    .buttom__studio,.p__studio,.div__studio{
        color:#ffff;
    }
    `;
